class GalleryController < ApplicationController
  layout 'gallery'

  def show
    @projects = ProjectsFinder.new.execute(current_user)
    render 'main.html.haml'
  end
end
