module AppearancesHelper
  def brand_item
    nil
  end

  def brand_title
    'GitLab Community Edition'
  end

  def brand_image
    nil
  end

  def brand_text
    nil
  end

  def brand_header_logo
    render 'shared/logo.svg'
  end

  def brand_header_isketa_logo
    render 'shared/isketa_logo.svg'
  end

end
